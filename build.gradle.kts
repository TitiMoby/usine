import com.moowork.gradle.node.npm.NpmTask

plugins {
  kotlin("jvm") version "1.3.61"
  application
  id("com.github.johnrengelman.shadow") version "5.0.0"
  id("com.moowork.node") version "1.3.1"
}

group = "org.titimoby"
version = "1.0.0-SNAPSHOT"

repositories {
  mavenLocal()
  jcenter()
}

val kotlinVersion = "1.3.20"
val vertxVersion = "3.8.5"
val junitJupiterEngineVersion = "5.4.0"

application {
  mainClassName = "io.vertx.core.Launcher"
}

val mainVerticleName = "org.titimoby.usine.MainVerticle"
val watchForChange = "src/**/*"
val doOnChange = "./gradlew classes"

dependencies {
  implementation("io.vertx:vertx-web:$vertxVersion")
  implementation("io.vertx:vertx-lang-kotlin:$vertxVersion")

  testImplementation("io.vertx:vertx-junit5:$vertxVersion")
  testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:$junitJupiterEngineVersion")
  testImplementation("org.junit.jupiter:junit-jupiter-api:$junitJupiterEngineVersion")
}

tasks {
  compileKotlin {
    kotlinOptions.jvmTarget = "1.8"
  }
  compileTestKotlin {
    kotlinOptions.jvmTarget = "1.8"
  }
  shadowJar {

    manifest {
      attributes["Main-Verticle"] = mainVerticleName
    }

    mergeServiceFiles {
      include("META-INF/services/io.vertx.core.spi.VerticleFactory")
    }
  }
  test {
    useJUnitPlatform()
    testLogging {
      events("PASSED", "FAILED", "SKIPPED")
    }
  }

  getByName<JavaExec>("run") {
    args = listOf("run", mainVerticleName, "--redeploy=${watchForChange}", "--launcher-class=${application.mainClassName}", "--on-redeploy=${doOnChange}")
  }
}

// Frontend to production


node {
  version = "10.16.0"
  npmVersion = "6.9.0"
  download = true
  nodeModulesDir = File("frontend")
}

val buildFrontend by tasks.creating(NpmTask::class) {
  setArgs(listOf("run", "build"))
  dependsOn("npmInstall")
}

val copyFrontendFiles by tasks.creating(Copy::class) {
  from("frontend/build")
  destinationDir = File("${buildDir}/classes/kotlin/main/webroot")
  dependsOn(buildFrontend)
}

val processResources by tasks.getting(ProcessResources::class) {
  dependsOn(copyFrontendFiles)
}


