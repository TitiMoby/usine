package org.titimoby.usine

import io.vertx.core.AbstractVerticle
import io.vertx.core.Promise
import io.vertx.ext.web.Router
import io.vertx.ext.web.RoutingContext
import io.vertx.ext.web.handler.StaticHandler

class MainVerticle : AbstractVerticle() {

  override fun start(startPromise: Promise<Void>) {
    val router: Router = Router.router(vertx)

    // Handle <server>/api/ping
    router["/api/ping"].handler { rc: RoutingContext ->
      rc.response().end("Pong!")
    }

    // Handle <server>/api/greetings
    router["/api/greetings"].handler { rc: RoutingContext ->
      rc.response().end("Hello from vert.x backend api!")
    }

    // used to handle frontend in production
    router.get().handler(StaticHandler.create())

    vertx
      .createHttpServer()
      .requestHandler (router)
      .listen(8888) { http ->
        when {
            http.succeeded() -> {
              startPromise.complete()
              println("HTTP server started on port 8888")
            }
            else -> {
              startPromise.fail(http.cause())
            }
        }
      }
  }
}
