import React, {useEffect, useState} from 'react';
import logo from './logo.svg';
import './App.css';

const App = () => {
  const [greetings, setGreetings] = useState<string>('Default before call API');
  useEffect(() => {
    console.log('Component mounted');
    fetch("/api/greetings")
      .then(res => res.text())
      .then(text => setGreetings(text))
    return () => {
      console.log('Component will be unmount');
    }
  });


  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo"/>
        <p>
          {greetings}
        </p>
      </header>
    </div>
  );
};

export default App;
